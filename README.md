##### 2d game written in Python with the SDL and OpenGL libraries.
Stay alive as long as possible, you have 3 lives.
Explications du code sous la forme d'un TP : [PDF](https://gitlab.com/ggcow/jeu-python-opengl/-/blob/master/explainations.pdf)

How to start :
1. For linux users, install the following SDL2 libraries
    * SDL2
    * SDL2_image
    * SDL2_ttf
    * SDL2_mixer
2. Install the python packages
    * pip install -r requirements.txt
3. Launch the main file
    * python main.py

Commands :
* Ctrl+F : Fullscreen
* Ctrl+S : Mute
* Arrow : Move
* TAB : Next level (cheat)

###### [Video link](https://youtu.be/fM9e1LMPsRc)
